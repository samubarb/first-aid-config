# First (-Aid) Config
First-**aid** configuration scripts for my Arch system.

## Introduction
One day I'll have to face an Arch re-install.
This repository is supposed to make a fresh install easier, *auto-configuring* some installed packages to be ready.
All the listed packages are the ones I personally use on my daily-driver workstation so, before using these scripts on your machine, be sure to personalize them as you wish.

## Structure
The folder contains:
- `pkglist.txt` is a list of all top packages from my actual Arch system.
- `src` is a folder that contains a shell script for every program to setup or configure.

## Usage
The two scripts in the main folder do all the job:
- `update-pkglist.sh` updates the `pkglist.txt` list from the actual working system. It's the output of `pacman -Qqet`
- `run-all.sh` executes **all** the scripts contained in `src` folder.
